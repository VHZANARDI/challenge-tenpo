package com.example.ChallengeTenpo.domain.mapper;

import com.example.ChallengeTenpo.domain.dto.HistoryUserDto;
import com.example.ChallengeTenpo.domain.dto.response.HistoryUserResponseDto;
import com.example.ChallengeTenpo.domain.entity.HistoryUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapperUtils {
    public static HistoryUserResponseDto toHistoryUserResponseDto(List<HistoryUser> historyUserList,Integer page,Integer limit){

        HistoryUserResponseDto responseDto = new HistoryUserResponseDto();
        List<HistoryUserDto> historys = new ArrayList<>();

        for(HistoryUser i : historyUserList){
            HistoryUserDto userDto = new HistoryUserDto();
            userDto = HistoryUserDto.builder()
                    .userId(i.getId())
                    .dateAction(i.getDateAction())
                    .action(i.getAction())
                    .build();
            historys.add(userDto);
        }

        responseDto.setPage(page);
        responseDto.setLimit(limit);
        responseDto.setHistory(historys);

        return responseDto;
    }
}
