package com.example.ChallengeTenpo.domain.error;

import com.example.ChallengeTenpo.domain.dto.ErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class ChallengerExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = ChallengeException.class)
    public ResponseEntity<?> handleException(ChallengeException exception) {

        ErrorDto errorDto = new ErrorDto(exception.getMessage(), exception.getStatusCode().value(), exception.getUriRequested());

        return new ResponseEntity<>(errorDto, exception.getStatusCode());
    }

}
