package com.example.ChallengeTenpo.domain.enums;

import lombok.Generated;

public enum HistoryAction {
    SUM,
    LOGIN,
    REGISTER;
}
