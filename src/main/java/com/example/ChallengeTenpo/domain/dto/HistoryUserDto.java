package com.example.ChallengeTenpo.domain.dto;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoryUserDto {

    private long userId;
    private String action;
    private Date dateAction;
}
