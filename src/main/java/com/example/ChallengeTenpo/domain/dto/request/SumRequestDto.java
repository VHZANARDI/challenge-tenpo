package com.example.ChallengeTenpo.domain.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SumRequestDto {

    private Integer number1;
    private Integer number2;
}
