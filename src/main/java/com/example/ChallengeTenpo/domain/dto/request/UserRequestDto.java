package com.example.ChallengeTenpo.domain.dto.request;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRequestDto {

    private String username;
    private String password;
}
