package com.example.ChallengeTenpo.domain.dto.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SumResponseDto {

    private String username;
    private Integer result;

}
