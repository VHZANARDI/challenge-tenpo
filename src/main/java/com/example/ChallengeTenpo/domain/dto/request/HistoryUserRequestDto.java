package com.example.ChallengeTenpo.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoryUserRequestDto {

    @JsonProperty("number_page")
    private Integer numberPage;
    private Integer limit;
}
