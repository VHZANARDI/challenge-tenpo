package com.example.ChallengeTenpo.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDto {

    private String message;
    @JsonProperty("status_code")
    private Integer statusCode;
    @JsonProperty("uri")
    private String uriRequested;
}
