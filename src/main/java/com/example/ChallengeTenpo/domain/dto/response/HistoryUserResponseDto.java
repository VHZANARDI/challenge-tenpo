package com.example.ChallengeTenpo.domain.dto.response;

import com.example.ChallengeTenpo.domain.dto.HistoryUserDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoryUserResponseDto {

    private List<HistoryUserDto> history;
    private Integer page;
    private Integer limit;
}
