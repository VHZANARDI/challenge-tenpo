package com.example.ChallengeTenpo.domain.service;

public interface LoginService {
    String auth(String username, String password);

    void saveUser(String username, String password);
}
