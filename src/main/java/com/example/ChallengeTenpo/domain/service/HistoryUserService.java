package com.example.ChallengeTenpo.domain.service;

import com.example.ChallengeTenpo.domain.enums.HistoryAction;

public interface HistoryUserService {
    void saveHistory(String username, HistoryAction sum);

    Object historyByUser(String username, Integer numberPage, Integer limit);
}
