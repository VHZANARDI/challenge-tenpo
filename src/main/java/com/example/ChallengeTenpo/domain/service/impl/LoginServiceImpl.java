package com.example.ChallengeTenpo.domain.service.impl;

import com.example.ChallengeTenpo.domain.entity.User;
import com.example.ChallengeTenpo.domain.error.ChallengeException;
import com.example.ChallengeTenpo.domain.repository.UserRepository;
import com.example.ChallengeTenpo.infrastructure.security.JwtUtils;
import com.example.ChallengeTenpo.domain.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;
    private final JwtUtils jwtUtils;

    @Override
    public String auth(String username, String password) {

        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new ChallengeException("User does not exist", HttpStatus.INTERNAL_SERVER_ERROR,"/login");
        }

        if (!BCrypt.checkpw(password.getBytes(), user.getPassword())) {
            throw new ChallengeException("Incorrect password", HttpStatus.INTERNAL_SERVER_ERROR,"/login");
        }

        return jwtUtils.getJWTToken(username,user.getId());
    }

    @Override
    public void saveUser(String username, String password) {

        User user = userRepository.findByUsername(username);

        if (user != null) {
            throw new ChallengeException("User already registered", HttpStatus.INTERNAL_SERVER_ERROR,"/register");
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(passwordEncoder.encode(password));

        userRepository.save(newUser);
    }
}
