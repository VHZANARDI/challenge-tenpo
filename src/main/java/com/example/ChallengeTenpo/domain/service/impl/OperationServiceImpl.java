package com.example.ChallengeTenpo.domain.service.impl;

import com.example.ChallengeTenpo.domain.service.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OperationServiceImpl implements OperationService {

    @Override
    public Integer sumNumbers(Integer number1, Integer number2) {

        return number1 + number2;
    }

}
