package com.example.ChallengeTenpo.domain.service;

public interface OperationService {
    Integer sumNumbers(Integer number1, Integer number2);
}
