package com.example.ChallengeTenpo.domain.service.impl;

import com.example.ChallengeTenpo.domain.entity.HistoryUser;
import com.example.ChallengeTenpo.domain.entity.User;
import com.example.ChallengeTenpo.domain.enums.HistoryAction;
import com.example.ChallengeTenpo.domain.error.ChallengeException;
import com.example.ChallengeTenpo.domain.mapper.MapperUtils;
import com.example.ChallengeTenpo.domain.repository.HistoryUserRepository;
import com.example.ChallengeTenpo.domain.repository.UserRepository;
import com.example.ChallengeTenpo.domain.service.HistoryUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HistoryUserServiceImpl implements HistoryUserService {

    private final HistoryUserRepository historyUserRepository;
    private final UserRepository userRepository;

    @Override
    public void saveHistory(String username, HistoryAction sum) {

        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new ChallengeException("User does not exist", HttpStatus.INTERNAL_SERVER_ERROR,"/history");
        }

        HistoryUser historyUser = new HistoryUser();
        historyUser.setUserId(user);
        historyUser.setAction(sum.toString());
        historyUser.setDateAction(new Date());

        historyUserRepository.save(historyUser);
    }

    @Override
    public Object historyByUser(String username, Integer numberPage, Integer limit) {

        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new ChallengeException("User does not exist", HttpStatus.INTERNAL_SERVER_ERROR,"/history");
        }

        Pageable pageable = PageRequest.of(numberPage, limit);
        Page<HistoryUser> historyUserPage = historyUserRepository.findByUserId(user, pageable);

        if(historyUserPage != null){

            return MapperUtils.toHistoryUserResponseDto(historyUserPage.getContent(),numberPage, limit);

        } else {

            throw new ChallengeException("History does not exist", HttpStatus.INTERNAL_SERVER_ERROR,"/history");
        }

    }
}
