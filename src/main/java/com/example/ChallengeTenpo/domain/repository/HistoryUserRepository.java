package com.example.ChallengeTenpo.domain.repository;

import com.example.ChallengeTenpo.domain.entity.HistoryUser;
import com.example.ChallengeTenpo.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface HistoryUserRepository extends JpaRepository<HistoryUser, Long> {

    Page<HistoryUser> findByUserId(User user, Pageable pageable);

}
