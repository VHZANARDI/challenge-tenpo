package com.example.ChallengeTenpo.infrastructure.security;

public interface SecurityConstants {
    String JWT_SECRET_KEY = "jwt.secret.key.challenge.tenpo.victor.hugo.zanardi";
    String PREFIX = "Bearer ";
    String HEADER_AUTH = "Authorization";
}
