package com.example.ChallengeTenpo.infrastructure.controller;

import com.example.ChallengeTenpo.domain.error.ChallengeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ChallengeTenpo.domain.dto.request.SumRequestDto;
import com.example.ChallengeTenpo.domain.dto.response.SumResponseDto;
import com.example.ChallengeTenpo.domain.enums.HistoryAction;
import com.example.ChallengeTenpo.domain.service.HistoryUserService;
import com.example.ChallengeTenpo.domain.service.OperationService;

import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OperationController {

    private final OperationService operationService;
    private final HistoryUserService historyUserService;

    @PostMapping("/sumNumbers")
    public ResponseEntity<?> sum(HttpServletRequest request, @RequestBody SumRequestDto sumRequestDto) {

        if(sumRequestDto != null && sumRequestDto.getNumber1() != null && sumRequestDto.getNumber2() != null) {

            String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

            Integer resultSum = operationService.sumNumbers(sumRequestDto.getNumber1(), sumRequestDto.getNumber2());
            historyUserService.saveHistory(username, HistoryAction.SUM);

            SumResponseDto responseDto = new SumResponseDto(username, resultSum);

            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        }
        throw new ChallengeException("Numbers not provided",HttpStatus.FORBIDDEN,"/sumNumbers");
    }
}
