package com.example.ChallengeTenpo.infrastructure.controller;

import com.example.ChallengeTenpo.domain.dto.response.LoginResponseDto;
import com.example.ChallengeTenpo.domain.dto.request.UserRequestDto;
import com.example.ChallengeTenpo.domain.enums.HistoryAction;
import com.example.ChallengeTenpo.domain.error.ChallengeException;
import com.example.ChallengeTenpo.domain.service.HistoryUserService;
import com.example.ChallengeTenpo.domain.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginController {

    private final LoginService service;
    private final HistoryUserService historyUserService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody UserRequestDto userRequestDto) {

       if(userRequestDto != null && userRequestDto.getUsername() != null && userRequestDto.getPassword() != null) {

           if (!userRequestDto.getUsername().isEmpty() && !userRequestDto.getPassword().isEmpty()) {

               final String token = service.auth(userRequestDto.getUsername(), userRequestDto.getPassword());

               LoginResponseDto loginResponseDto = new LoginResponseDto();
               loginResponseDto.setUsername(userRequestDto.getUsername());
               loginResponseDto.setToken(token);

               historyUserService.saveHistory(userRequestDto.getUsername(), HistoryAction.LOGIN);

               return new ResponseEntity<>(loginResponseDto, HttpStatus.OK);
           }
       }
       throw new ChallengeException("Username o Password not provided",HttpStatus.FORBIDDEN,"/login");

    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserRequestDto userRequestDto) {

        if(userRequestDto != null && userRequestDto.getUsername() != null && userRequestDto.getPassword() != null) {

            if (!userRequestDto.getUsername().isEmpty() && !userRequestDto.getPassword().isEmpty()) {

                service.saveUser(userRequestDto.getUsername(), userRequestDto.getPassword());

                historyUserService.saveHistory(userRequestDto.getUsername(), HistoryAction.REGISTER);

                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        }
        throw new ChallengeException("Username o Password not provided",HttpStatus.FORBIDDEN,"/register");
    }
}
