package com.example.ChallengeTenpo.infrastructure.controller;

import com.example.ChallengeTenpo.domain.dto.request.HistoryUserRequestDto;
import com.example.ChallengeTenpo.domain.error.ChallengeException;
import com.example.ChallengeTenpo.domain.service.HistoryUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HistoryUserController {

    private final HistoryUserService historyUserService;

    @GetMapping("/history")
    public ResponseEntity<?> sum(@RequestBody HistoryUserRequestDto requestDto) {

        if(requestDto != null && requestDto.getNumberPage() != null && requestDto.getLimit() != null) {
            String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

            return new ResponseEntity<>(historyUserService.historyByUser(username, requestDto.getNumberPage(), requestDto.getLimit()), HttpStatus.OK);
        }
        throw new ChallengeException("Number page or limit not provided",HttpStatus.FORBIDDEN,"/history");
    }
}
