# Challeger Tenpo

## Enunciado 📄

1. Debes desarrollar una API REST en Spring Boot con las siguientes
   funcionalidades:

   a. Sign up usuarios.

   b. Login usuarios vía token JWT.

   c. Sumar dos números. Este endpoint debe retornar el resultado de la
   suma y puede ser consumido solo por usuarios logueados.

   d. Historial de todas las operaciones por usuario. Responder en Json,
   con data paginada y el límite.

   e. Logout usuarios.

   f. El historial y la información de los usuarios se debe almacenar en
   una database PostgreSQL.

   g. Incluir errores http. Mensajes y descripciones para la serie 400.

2. Esta API debe ser desplegada en un docker container. Este docker
   puede estar en un dockerhub público. La base de datos también debe
   correr en un contenedor docker.

3. Debes agregar un Postman Collection o Swagger para que probemos tu API.

4. Tu código debe estar disponible en un repositorio público, junto
   con las instrucciones de cómo desplegar el servicio y cómo utilizarlo.

---

## Aclaraciones
No realice el "logout de usuario" ya que jwt nos permite mantener una actividad por X
cantidad de tiempo. La única solución para realizar un logout sería guardar dichos
token en base de datos y luego cambiar la expiración o devolver un token invalido.
No me pareció una solución práctica.

---
## Stack Tecnológico 🛠️

* Java 11 JDK
* Apache Maven 3.5.x en adelante
* [Spring boot](https://spring.io/projects/spring-boot) - Framework de java
---
## Pre-Requisitos 📋

_Software necesarios:_
* JDK
* Git
* Maven 3.0.0 o superior
* Docker
* Docker compose (https://docs.docker.com/compose/install/)
---
## Instalación 🔧
#### 1 - Clonar dicho repositorio

#### 2 - Localizar raiz del proyecto y realizar un pull al docker publico

    docker-compose pull

#### 3 - Correr el proyecto

    docker-compose up

#### 4 - Importar en postman el archivo ChallengeTenpo.postman_collection.json ubicado en la raiz de este proyecto

#### 5 - Consumir los endpoints de la siguiente manera para probar el flujo:

* a. "/register"

* b. "/login"

* c. "/sumNumbers"

* d. "/history"

####PD: Se agrego swagger para probar desde el buscardo http://localhost:8080/swagger-ui.html pero recomiendo probar desde el postman indicado


---
## Autor ✒

* **Victor Hugo Zanardi** - [Victor](https://gitlab.com/VHZANARDI)

